Requires Bootstrap 3.0.0

Changelog 0.2.0
---------------

- Support for Python 3 and Django 1.9 (maintaining compatibility with 2.7.x and Django 1.6).
- Removed HtmlFileDropZone from forms_extra.py, as it depends on django_dropzone (should be included in django_dropzone 
package as an extra).
- Added missing static files.
- Added tests (basic).
- Added global utils.py and view-specific utils.py.
- Added missing requirements to requirements.txt.
- Minor typo fixes and code-styling.
- Messages and UI elements are now in English (translatable).
- validation.js.html: Change 'f_error' class to 'has-error' not to depend on external CSS.

TODO
----

- Remove dependency on djangoformsetjs?


Compatibility break changed with Finaer bundled version
-------------------------------------------------------

- Removed HtmlFileDropZone from forms_extra.py, as it depends on django_dropzone (should be included in django_dropzone 
package as an extra)
- Moved InlineFormsetSaver from utils.py to views/utils.py (makes more sense).
- Beware that is NOT views/form_validation.py (it's legacy code, not needed anymore?)
- Removed import_class from utils.py (needed only by form_validation.py, which is not included anymore)
- Moved static files needed by admin-objects from Finaer global static to static folder in the app.
- Messages and UI elements are now in English. You should provide a locale for Spanish support.