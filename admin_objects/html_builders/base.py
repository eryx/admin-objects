import cgi
import re
from xml.etree.ElementTree import tostring, Element

from django.contrib.humanize.templatetags.humanize import intcomma
from django.template import Template, Context, RequestContext
from django.utils import formats
from django.utils import timezone
from django.utils.encoding import force_text
from past.builtins import long
from six import iteritems

from admin_objects.js_builder.base import JSImporter, JSTemplate


class TemplateElement(object):
    # initialization protocol

    def __init__(self, **kwargs):
        self.global_context = {}
        for key, value in iteritems(kwargs):
            self.global_context[self._context_arg_key(key)] = value

        self.global_template_tags = set()

    # global attributes protocol

    def template_tags(self):
        return self.global_template_tags

    # build protocol

    def build(self):
        raise Exception("Subclass responsibility.")

    # render protocol

    def render(self, request=None):
        elem_to_render = self.build()
        if isinstance(elem_to_render, Element):
            text_to_render = tostring(elem_to_render).decode("utf-8")
            # FIXME: Ugly hack to undo the replacement done in _escape_cdata function (xml.etree.ElementTree).
            # This is needed because if not html tags rendered beforehand (like JS objects defined in js_builder/base.py)
            # do not render correctly in this final rendering.
            text_to_render = text_to_render.replace("&lt;", "<").replace("&gt;", ">")
        else:
            text_to_render = elem_to_render

        if self.global_template_tags:
            template_tags_load = '{%% load %(template_tags)s %%}' % {
                'template_tags': ' '.join(self.global_template_tags)}
            text_to_render = template_tags_load + text_to_render

        t = Template(text_to_render)
        context = RequestContext(request, self.global_context) if request else Context(self.global_context)
        return t.render(context)

    def __unicode__(self):
        return self.render()

    # access protocol

    def context_arg(self, arg_name):
        return self._context_arg_key(arg_name)

    def id(self):
        return str(id(self))

    # private helpers protocol

    def _context_arg_key(self, arg_name):
        return '%(prefix)s_%(name)s' % {'prefix': self.id(), 'name': arg_name}


class HtmlElement(TemplateElement):
    # initialization protocol

    def __init__(self, element_id=None, content=None, title=None, data_attribs=None, **kwargs):
        super(HtmlElement, self).__init__(**kwargs)

        self.custom_id = element_id
        self.title = title
        self.custom_css_classes = []
        self.custom_attrib = {}

        if data_attribs:
            for key, value in iteritems(data_attribs):
                data_key = 'data-%s' % key
                self.add_attrib({data_key: str(value)})

        if content:
            try:
                iter(content)
            except TypeError:
                content = [content]

            for template_element in content:
                if not isinstance(template_element, TemplateElement):
                    raise Exception("'content' argument should be an object (or list) of type TemplateElement")

                self.global_context.update(template_element.global_context)
                self.global_template_tags.update(set(template_element.template_tags()))

        self.content = content
        self._escape_script_tags = False

    # assertions protocol

    def assert_only_n_contents_exist(self, n):
        if not self.content or len(self.content) != n:
            raise Exception("%s instances must have %s 'contents'." % (self.__class__.__name__, n))

    def assert_content_is_not_empty(self):
        assert self.content

    # global attributes protocol

    def tag(self):
        raise Exception("Subclass responsibility.")

    def attrib(self):
        return {}

    def css_classes(self):
        return []

    def js_imports(self):
        return []

    # build protocol

    def build(self):
        xml_element = self.build_xml_element()

        if self.content:
            for template_element in self.content:
                builded_template_element = template_element.build()
                if isinstance(builded_template_element, Element):
                    xml_element.append(builded_template_element)
                else:
                    xml_element.text = builded_template_element

        return xml_element

    # access protocol

    def has_to_escape_script_tags(self):
        return self._escape_script_tags

    def element_id(self):
        return self.custom_id or self.default_element_id()

    # modification protocol for wrappers (to be used ONLY when composing template elements, not in View classes)

    def escape_script_tags(self):
        self._escape_script_tags = True
        if self.content:
            for template_element in self.content:
                if isinstance(template_element, HtmlElement):
                    template_element.escape_script_tags()

    def add_css_classes(self, css_classes):
        self.custom_css_classes += css_classes
        return self

    def add_attrib(self, attrib):
        self.custom_attrib.update(attrib)

    # render protocol

    def render_js(self):
        js_code = ''
        for js_file in self.all_js_imports():
            js_code += js_file.render()

        return js_code

    # private helpers protocol

    def default_element_id(self):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1-\2', self.__class__.__name__)
        class_name_hyphened = re.sub('([a-z0-9])([A-Z])', r'\1-\2', s1).lower()  # TemplateElement -> template-element
        return class_name_hyphened + '-' + self.id()

    def all_js_imports(self):
        js_importer = JSImporter(self.js_imports())
        if self.content:
            for template_element in self.content:
                if isinstance(template_element, HtmlElement):
                    js_importer.add(template_element.all_js_imports())
        return js_importer

    def build_xml_element(self):
        return Element(self.tag(), attrib=self.all_attrib())

    def all_attrib(self):
        all_attrib = self.attrib()
        all_attrib.update({'id': self.element_id()})

        if self.title:
            all_attrib.update({'title': self.title})

        if self.all_css_classes():
            all_attrib.update({'class': ' '.join(self.all_css_classes())})

        all_attrib.update(self.custom_attrib)  # custom attrib can override anything
        return all_attrib

    def all_css_classes(self):
        all_css_classes = self.css_classes()

        all_css_classes += self.custom_css_classes  # custom css_classes go last
        return all_css_classes


class TextElement(TemplateElement):
    def __init__(self, text, if_empty='-', strong=False):
        super(TextElement, self).__init__()

        self.strong = strong
        if text:
            self.text = text
        else:
            self.text = if_empty

    def build(self):
        text_element = cgi.escape(force_text(self.text))

        if self.strong:
            element_wrapper = Element('strong')  # FIXME: TextElement is NOT an HtmlElement by design
            element_wrapper.text = text_element
            text_element = element_wrapper

        return text_element


class DateTimeElement(TemplateElement):
    def __init__(self, datetime_object):
        super(DateTimeElement, self).__init__()
        self.datetime_object = datetime_object

    def build(self):
        # https://docs.djangoproject.com/en/dev/ref/settings/#short-datetime-format
        return formats.date_format(timezone.localtime(self.datetime_object), "SHORT_DATETIME_FORMAT")


class MoneyElement(TemplateElement):
    def __init__(self, amount, if_empty='-'):
        super(MoneyElement, self).__init__()
        if amount:
            self.amount = amount
        else:
            self.amount = if_empty

    def build(self):
        if isinstance(self.amount, (int, long, float)):
            return force_text('$ %s' % intcomma(self.amount))
        else:
            return force_text(self.amount)


class HtmlAnchor(HtmlElement):
    def __init__(self, href, target='_self', content=None, **kwargs):
        super(HtmlAnchor, self).__init__(content=content, **kwargs)
        self.href = href
        self.target = target

    def tag(self):
        return 'a'

    def attrib(self):
        attribs = super(HtmlAnchor, self).attrib()
        attribs.update({'href': self.href, 'target': self.target})
        return attribs


class HtmlHeading(HtmlElement):
    def __init__(self, level, content, **kwargs):
        self.level = level

        super(HtmlHeading, self).__init__(content=content, **kwargs)

    def tag(self):
        return 'h%s' % self.level


class HtmlPageHeader(HtmlHeading):
    def css_classes(self):
        return ['page-header']


class HtmlUnorderedList(HtmlElement):
    def __init__(self, list_decoration=True, **kwargs):
        self.list_decoration = list_decoration

        super(HtmlUnorderedList, self).__init__(**kwargs)

    def attrib(self):
        if not self.list_decoration:
            return {'style': 'list-style-type: none; padding: 0;'}

        return super(HtmlUnorderedList, self).attrib()

    def tag(self):
        return 'ul'


class HtmlListItem(HtmlElement):
    def tag(self):
        return 'li'


class HtmlParagraph(HtmlElement):
    def tag(self):
        return 'p'


class JSChainedElement(HtmlElement):
    """
        Decorator to link multiple template elements through JS.

        linked_to: dict of named template_elements.
        i.e. {'button_1': button_template_element, 'name_input': input_template_element}
    """

    def __init__(self, linked_to, js_template_path, content, **kwargs):
        self.linked_to = linked_to
        self.js_template_path = js_template_path

        super(JSChainedElement, self).__init__(content=content, **kwargs)

        self.assert_only_n_contents_exist(n=1)

    def tag(self):
        return self.content_element().tag()

    def content_element(self):
        return self.content[0]

    def build(self):
        return self.content_element().build()

    def element_id(self):
        return self.content_element().element_id()

    def js_imports(self):
        template_context = {name: template_element.element_id() for name, template_element in self.linked_to.items()}
        template_context.update({'element': self.content_element().element_id()})
        return [JSTemplate(self.js_template_path, context=template_context)]
