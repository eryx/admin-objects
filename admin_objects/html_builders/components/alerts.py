from admin_objects.html_builders.base import HtmlElement


class HtmlAlert(HtmlElement):
    def __init__(self, type, **kwargs):
        self._validate_alert_type(type)
        super(HtmlAlert, self).__init__(**kwargs)
        self.type = type

    def css_classes(self):
        return super(HtmlAlert, self).css_classes() + ['alert', 'alert-' + self.type]

    def tag(self):
        return 'div'

    def _validate_alert_type(self, type):
        if type not in HtmlAlert.valid_alert_types():
            raise Exception('%s is not a valid alert type. ' % type)

    @classmethod
    def valid_alert_types(cls):
        return ['success', 'warning', 'info', 'danger']
