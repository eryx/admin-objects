from admin_objects.html_builders.base import HtmlUnorderedList, HtmlElement, HtmlListItem
from admin_objects.html_builders.css.buttons import HtmlDefaultButton, HtmlButtonGroup


class HtmlDropdownMenu(HtmlUnorderedList):
    def css_classes(self):
        return ['dropdown-menu']


class DropdownToggleButton(HtmlDefaultButton):
    def css_classes(self):
        css_classes = super(DropdownToggleButton, self).css_classes()
        css_classes += ['dropdown-toggle']
        return css_classes

    def attrib(self):
        attribs = super(DropdownToggleButton, self).attrib()

        attribs.update({
            'data-toggle': 'dropdown'
        })

        return attribs


class Caret(HtmlElement):
    def tag(self):
        return 'span'

    def css_classes(self):
        return ['caret']


class HtmlButtonDropdown(HtmlButtonGroup):
    def __init__(self, button_content, dropdown_content, direction='down'):
        self.direction = direction

        content = self._button(button_content)
        content += self._dropdown_menu(dropdown_content)

        super(HtmlButtonDropdown, self).__init__(content=content)

    def _button(self, button_content):
        return [DropdownToggleButton(content=button_content)]

    def _dropdown_menu(self, dropdown_content):
        return [HtmlDropdownMenu(list_decoration=False,
                                 content=[HtmlListItem(content=element) for element in dropdown_content])]
