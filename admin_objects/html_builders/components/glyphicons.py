from admin_objects.html_builders.base import HtmlElement


class HtmlIcon(HtmlElement):
    def __init__(self, icon_class, **kwargs):
        self.icon_class = icon_class

        super(HtmlIcon, self).__init__(**kwargs)

    def tag(self):
        return 'span'

    def css_classes(self):
        return ['glyphicon', 'glyphicon-' + self.icon_class]

    def build(self):
        xml_element = self.build_xml_element()
        xml_element.text = ' '

        return xml_element
