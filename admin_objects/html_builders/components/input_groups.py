from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement


class HtmlInputGroup(HtmlElement):
    ADDON_POSITION_RIGHT = 'right'
    ADDON_POSITION_LEFT = 'left'

    def __init__(self, addon_position=ADDON_POSITION_RIGHT, content=None, **kwargs):
        self.assert_two_content_elements_exist(content)

        self.addon_position = addon_position

        super(HtmlInputGroup, self).__init__(content=content, **kwargs)

    def assert_two_content_elements_exist(self, content):
        if not content or len(content) != 2:
            raise Exception(u'Las instancias de HtmlInputGroup deben tener 2 elementos como contenido.')

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['input-group']

    def build_addon_from(self, element):
        input_group_addon = Element('span', attrib={'class': 'input-group-addon'})

        input_group_addon.append(element)

        return input_group_addon

    def build(self):
        xml_element = self.build_xml_element()
        first_element = self.content[0].build()
        second_element = self.content[1].build()

        if self.addon_position == self.ADDON_POSITION_LEFT:
            xml_element.append(self.build_addon_from(first_element))
            xml_element.append(second_element)
        else:
            xml_element.append(first_element)
            xml_element.append(self.build_addon_from(second_element))

        return xml_element
