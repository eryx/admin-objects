from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement


class HtmlListGroup(HtmlElement):
    def tag(self):
        return 'ul'

    def css_classes(self):
        return ['list-group']


class HtmlListGroupItem(HtmlElement):
    def tag(self):
        return 'li'

    def css_classes(self):
        return ['list-group-item']

    def build(self):
        xml_element = self.build_xml_element()
        list_group_item_header = Element('h4', attrib={'class': 'list-group-item-heading label label-info'})
        list_group_item_header.text = '{{ %s }}' % self.context_arg('heading')

        xml_element.append(list_group_item_header)

        for html_elements in self.content:
            xml_element.append(html_elements.build())

        return xml_element
