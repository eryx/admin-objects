# coding=utf-8
from admin_objects.html_builders.base import HtmlElement


# Reference
# http://getbootstrap.com/components/#panels

class HtmlPanel(HtmlElement):
    """
    content: Must have a HtmlPanelBody inside. HtmlPanelHeading is optional.
    """

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['panel', 'panel-default']


class HtmlPanelBody(HtmlElement):
    def tag(self):
        return 'div'

    def css_classes(self):
        return ['panel-body']


class HtmlPanelHeading(HtmlElement):
    def tag(self):
        return 'div'

    def css_classes(self):
        return ['panel-heading']
