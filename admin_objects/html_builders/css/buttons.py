from admin_objects.html_builders.base import HtmlElement, HtmlAnchor


class HtmlAnchorButton(HtmlAnchor):
    def css_classes(self):
        css_classes = super(HtmlAnchorButton, self).css_classes()
        css_classes += ['btn', 'btn-default']
        return css_classes


class HtmlButton(HtmlElement):
    def tag(self):
        return 'button'


class HtmlDefaultButton(HtmlButton):
    def css_classes(self):
        css_classes = super(HtmlDefaultButton, self).css_classes()
        css_classes += ['btn', 'btn-default']
        return css_classes


class HtmlSubmitButton(HtmlButton):
    def attrib(self):
        attribs = super(HtmlSubmitButton, self).attrib()
        attribs.update({'type': 'submit'})
        return attribs


class HtmlSubmitButtonDefault(HtmlSubmitButton):
    def css_classes(self):
        css_classes = super(HtmlSubmitButtonDefault, self).css_classes()
        css_classes += ['btn', 'btn-default']
        return css_classes


class HtmlSuccessButton(HtmlButton):
    def css_classes(self):
        css_classes = super(HtmlSuccessButton, self).css_classes()
        css_classes += ['btn', 'btn-success']
        return css_classes


class HtmlDangerButton(HtmlButton):
    def css_classes(self):
        css_classes = super(HtmlDangerButton, self).css_classes()
        css_classes += ['btn', 'btn-danger']
        return css_classes


class HtmlWarningButton(HtmlButton):
    def css_classes(self):
        css_classes = super(HtmlWarningButton, self).css_classes()
        css_classes += ['btn', 'btn-warning']
        return css_classes


class HtmlButtonGroup(HtmlElement):
    def tag(self):
        return 'div'

    def css_classes(self):
        return ['btn-group']
