# coding=utf-8
from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement


class HtmlFormGroup(HtmlElement):
    def __init__(self, label, **kwargs):
        super(HtmlFormGroup, self).__init__(label=label, **kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['form-group']

    def build_label(self):
        label = Element('label', attrib={'for': '{{ %s }}' % self.context_arg('label'),
                                         'class': 'control-label'})
        label.text = '{{ %s }}' % self.context_arg('label')
        return label

    def build(self):
        xml_element = self.build_xml_element()
        xml_element.append(self.build_label())

        for content in self.content:
            xml_element.append(content.build())

        return xml_element


class HtmlReadOnlyFormGroup(HtmlFormGroup):
    def attrib(self):
        attribs = super(HtmlReadOnlyFormGroup, self).attrib()
        attribs.update({'readonly': ''})

        return attribs


class HtmlInput(HtmlElement):
    # FIXME: HtmlInput in bootstrap is NOT wrapped in a div element. Should refactor TemplateElements to be able to
    # render a templatetag without parent tag (HtmlInput would inherit from something else, not current HtmlElement)?

    def __init__(self, input_tag, placeholder=u'', **kwargs):
        self.input_tag = input_tag
        self.placeholder = placeholder
        super(HtmlInput, self).__init__(input_tag=input_tag, **kwargs)

    def tag(self):
        return 'div'

    def template_tags(self):
        return ['widget_tweaks']

    def build(self):
        xml_element = self.build_xml_element()

        form_input = '{{ %s|add_class:"form-control"|attr:"placeholder:%s" }}' % (
            self.context_arg('input_tag'), self.placeholder)
        xml_element.text = form_input

        return xml_element


# TODO: Hack until we know how to refactor HtmlInput
class HtmlInputForHtmlTables(HtmlInput):
    def css_classes(self):
        return []


class HtmlHiddenInput(HtmlInput):
    """
        Hidden input, useful for non graphics related html like hidden tokens.
    """

    def attrib(self):
        return {'style': 'display: none;'}


class HtmlRadioButtonInput(HtmlElement):
    def tag(self):
        return 'div'

    def build(self):
        xml_element = self.build_xml_element()

        form_input = '{{ %s }}' % self.context_arg('input_tag')
        xml_element.text = form_input

        return xml_element


class HtmlCheckbox(HtmlElement):
    def __init__(self, label, input_tag, **kwargs):
        self.label = label

        super(HtmlCheckbox, self).__init__(input_tag=input_tag, **kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['checkbox']

    def build(self):
        xml_element = self.build_xml_element()

        label = Element('label', attrib={'class': 'control-label'})
        label.text = '{{ %s }} %s' % (self.context_arg('input_tag'), self.label)

        xml_element.append(label)

        return xml_element


class HtmlStaticControl(HtmlElement):
    def __init__(self, text, empty_text='-', **kwargs):
        self.empty_text = empty_text
        super(HtmlStaticControl, self).__init__(text=text, **kwargs)

    def tag(self):
        return 'div'

    def build(self):
        xml_element = self.build_xml_element()

        xml_element.append(self.build_text_content())

        return xml_element

    def build_text_content(self):
        text_content_element = Element('p', attrib={'class': 'form-control-static'})
        text_content_element.text = '{{ %s|default:"%s" }}' % (self.context_arg('text'), self.empty_text)

        return text_content_element


class HtmlRadio(HtmlElement):
    def __init__(self, input_choice, **kwargs):
        super(HtmlRadio, self).__init__(input_choice=input_choice, **kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['radio']

    def build(self):
        xml_element = self.build_xml_element()

        label = Element('label', attrib={'class': 'control-label'})
        label.text = '{{ %(input_choice)s.tag }} {{ %(input_choice)s.choice_label }}' % {
            'input_choice': self.context_arg('input_choice')}

        xml_element.append(label)

        return xml_element
