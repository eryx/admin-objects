from admin_objects.html_builders.base import HtmlElement


class HtmlGridContainer(HtmlElement):
    """
        Bootstrap grid system rows container.
    """

    def css_classes(self):
        return ['container']

    def tag(self):
        return 'div'


class HtmlRow(HtmlElement):
    """
        Bootstrap grid system row.
    """

    def css_classes(self):
        return ['row']

    def tag(self):
        return 'div'


class HtmlColumn(HtmlElement):
    """
        Bootstrap grid system column.
        units_wide_by_size: dict of column units by size.
        i.e: { 'sm': 2, 'm': 4 }
    """

    def __init__(self, units_wide_by_size, **kwargs):
        self.assert_valid_dimensions(units_wide_by_size)
        self.units_wide_by_size = units_wide_by_size

        super(HtmlColumn, self).__init__(**kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['col-%s-%s' % (size, units_wide) for size, units_wide in self.units_wide_by_size.items()]

    def assert_valid_size(self, size):
        if size not in ['xs', 'sm', 'md', 'lg']:
            raise Exception(u'%s is not a valid column size' % size)

    def assert_valid_units_wide(self, units_wide):
        if not 1 <= float(units_wide) <= 12:
            raise Exception(u'Units must be between 1 and 12')

    def assert_valid_dimensions(self, units_wide_by_size):
        for size, units_wide in units_wide_by_size.items():
            self.assert_valid_size(size)
            self.assert_valid_units_wide(units_wide)
