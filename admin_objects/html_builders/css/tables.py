from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement


class HtmlTable(HtmlElement):
    def tag(self):
        return 'table'

    def css_classes(self):
        return ['table']


class HtmlTableHead(HtmlElement):
    def __init__(self, headers, **kwargs):
        self.headers = headers
        super(HtmlTableHead, self).__init__(content=None, **kwargs)

    def tag(self):
        return 'thead'

    def build(self):
        xml_element = self.build_xml_element()

        table_row = Element('tr')
        for header in self.headers:
            table_head_cell = Element('th')
            table_head_cell.text = header
            table_row.append(table_head_cell)

        xml_element.append(table_row)

        return xml_element


class HtmlTableBody(HtmlElement):
    def tag(self):
        return 'tbody'


class HtmlTableData(HtmlElement):
    def tag(self):
        return 'td'


class HtmlTableDataRow(HtmlElement):
    def __init__(self, content=None, **kwargs):
        wrapped_content = []
        for template_element in content:
            wrapped_content.append(HtmlTableData(content=template_element))
        super(HtmlTableDataRow, self).__init__(content=wrapped_content, **kwargs)

    def tag(self):
        return 'tr'
