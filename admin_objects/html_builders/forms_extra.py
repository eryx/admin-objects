from django.utils.translation import ugettext as _

from admin_objects.html_builders.base import HtmlElement
from admin_objects.js_builder.base import JSFile, JSTemplate, JSInline
from admin_objects.templatetags.jquery_validation import FormValidationOptionBuilder


class HtmlFormWithValidation(HtmlElement):
    def __init__(self, forms_validator, **kwargs):
        self.forms_validator = forms_validator
        super(HtmlFormWithValidation, self).__init__(**kwargs)

    def tag(self):
        return 'form'

    def attrib(self):
        return {'method': 'post', 'class': 'validate-form', 'role': 'form'}

    def js_imports(self):
        form_validation_builder = FormValidationOptionBuilder(self.forms_validator.forms_list())
        validate_settings = {'form_id': self.element_id(),
                             'rules': form_validation_builder.build_rules(),
                             'messages': form_validation_builder.build_messages(),
                             'needs_ajax_validation': self.forms_validator.validates_through_ajax(),
                             'validation_error_message': self.forms_validator.__class__.fields_error_message(),
                             'validation_in_progress_message':
                                 _(u"Performing validation... please wait."),
                             'validation_fail_message':
                                 _(u"There was an error during the validation process. Please try again.")}
        return [JSFile("js/query-string.js"),
                JSFile("lib/validation/jquery.validate.min.js"),
                JSFile("lib/validation/additional-methods.js"),
                JSFile("lib/sticky/sticky.js"),
                JSTemplate("forms/validation.js.html", validate_settings)]

    def build(self):
        xml_element = self.build_xml_element()
        xml_element.text = '{% csrf_token %}'

        for html_element in self.content:
            xml_element.append(html_element.build())

        return xml_element


class HtmlDateInput(HtmlElement):
    def __init__(self, input_tag, **kwargs):
        self.input_tag = input_tag

        super(HtmlDateInput, self).__init__(input_tag=input_tag, **kwargs)

    def tag(self):
        return 'div'

    def build(self):
        xml_element = self.build_xml_element()

        form_input = '{{ %s|add_class:"form-control" }}' % self.context_arg('input_tag')
        xml_element.text = form_input

        if self.has_to_escape_script_tags():
            # If it has to escape script tags it means JS should be inline because it's going to be loaded
            # dynamically when this form group is loaded into the DOM (e.g. after a '+' click in a formset)
            script_calendar = JSInline(
                "$('input#%(input_tag_id)s').datepicker({format: 'dd/mm/yyyy'});" % {
                    'input_tag_id': self.input_tag.auto_id},
                escape_tag=True).render()
            xml_element.tail = script_calendar

        return xml_element

    def js_imports(self):
        js_imports = [JSFile('lib/datepicker/bootstrap-datepicker.min.js')]
        if not self.has_to_escape_script_tags():
            # noinspection PyTypeChecker
            js_imports.append(JSInline(
                "$(\"input#%(input_tag_id)s\").datepicker({format: 'dd/mm/yyyy'});" % {
                    'input_tag_id': self.input_tag.auto_id}))
        return js_imports


class HtmlDateTimeInput(HtmlElement):
    def __init__(self, input_tag, **kwargs):
        self.input_tag = input_tag

        super(HtmlDateTimeInput, self).__init__(input_tag=input_tag, **kwargs)

    def tag(self):
        return 'div'

    def build(self):
        xml_element = self.build_xml_element()

        form_input = '{{ %s|add_class:"form-control" }}' % self.context_arg('input_tag')
        xml_element.text = form_input

        if self.has_to_escape_script_tags():
            # If it has to escape script tags it means JS should be inline because it's going to be loaded
            # dynamically when this form group is loaded into the DOM (e.g. after a '+' click in a formset)
            script_calendar = JSInline(
                "$('input#%(input_tag_id)s').datetimepicker({format: 'DD/MM/YYYY HH:mm'});" % {
                    'input_tag_id': self.input_tag.auto_id},
                escape_tag=True).render()
            xml_element.tail = script_calendar

        return xml_element

    def js_imports(self):
        js_imports = [JSFile('lib/datetimepicker/moment.min.js'),
                      JSFile('lib/datetimepicker/bootstrap-datetimepicker.min.js')]
        if not self.has_to_escape_script_tags():
            # noinspection PyTypeChecker
            js_imports.append(JSInline(
                "$(\"input#%(input_tag_id)s\").datetimepicker({format: 'DD/MM/YYYY HH:mm'});" % {
                    'input_tag_id': self.input_tag.auto_id}))
        return js_imports
