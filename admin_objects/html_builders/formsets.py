# coding=utf-8
from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement, TextElement
from admin_objects.html_builders.components.panels import HtmlPanel, HtmlPanelHeading, HtmlPanelBody
from admin_objects.js_builder.base import JSFile, JSTemplate, JSInline
from admin_objects.templatetags.jquery_validation import formset_dynamic_rules
from django.utils.translation import ugettext as _


class HtmlFormset(HtmlElement):
    """
    HtmlFormset basic structure

    formset_prefix: value of 'prefix' attribute of related Django Formset
    formset_management_form: value of 'management_form' attribute of related Django Formset
    content: should include
        [HtmlFormsetBody, HtmlFormsetEmptyFormTemplate]
    """

    def tag(self):
        return 'div'

    def attrib(self):
        return {'data-formset-prefix': self.formset_prefix}

    def css_classes(self):
        return ['well']

    def __init__(self, formset_prefix, formset_management_form, show_add=True, content=None, **kwargs):
        self.formset_prefix = formset_prefix
        self.show_add_button = show_add
        super(HtmlFormset, self).__init__(content=content, formset_management_form=formset_management_form, **kwargs)

    def formset_add_button(self):
        formset_add_button = Element('button', attrib={'type': 'button', 'class': 'btn btn-default',
                                                       'data-formset-add': ''})
        plus_sign_icon = Element('span', attrib={'class': 'glyphicon glyphicon-plus-sign'})
        plus_sign_icon.text = ' '
        formset_add_button.append(plus_sign_icon)
        return formset_add_button

    def build(self):
        xml_element = self.build_xml_element()
        xml_element.text = '{{ %s }}' % self.context_arg('formset_management_form')

        for html_formset_forms in self.content:
            xml_element.append(html_formset_forms.build())

        if self.show_add_button:
            xml_element.append(self.formset_add_button())

        return xml_element

    def js_imports(self):
        # js file provided by django-formset-js
        return [JSFile("js/jquery.formset.js"), JSInline("$('#%s').formset();" % self.element_id())]

    def template_tags(self):
        return ['widget_tweaks']


class HtmlFormsetBody(HtmlElement):
    """
    HtmlFormsetBody basic structure

    content: should include a list of HtmlFormsetForm (or an HtmlFormsetEmptyForm for initial 'extra' empty forms)
    """

    def tag(self):
        return 'div'

    def attrib(self):
        return {'data-formset-body': ''}

    def build(self):
        xml_element = super(HtmlFormsetBody, self).build()
        xml_element.text = ' '  # empty html formset body should be "<div> </div>" instead of "<div />"
        return xml_element


class HtmlFormsetForm(HtmlPanel):
    def attrib(self):
        attrib = super(HtmlFormsetForm, self).attrib()
        attrib.update({'data-formset-form': ''})
        return attrib


class HtmlFormsetEmptyForm(HtmlFormsetForm):
    """
    An empty formset form (extra initial forms, configured by the 'extra' parameter of formset_factory).
    """

    def js_imports(self):
        return [JSTemplate("forms/delete_formset_form.js.html", {'element_id': self.element_id()}), ]


class HtmlFormsetEmptyFormHeading(HtmlPanelHeading):
    """
    An special panel heading with a Delete button for dynamic deletion of extra empty formset forms.
    """

    def build(self):
        xml_element = super(HtmlFormsetEmptyFormHeading, self).build()

        remove_button = Element('button', attrib={'type': 'button', 'class': 'btn btn-default btn-xs remove pull-right',
                                                  'title': _(u'Delete'), 'data-formset-delete-button': ''})
        remove_button_icon = Element('span', attrib={'class': 'glyphicon glyphicon-remove'})
        remove_button.append(remove_button_icon)

        xml_element.append(remove_button)

        return xml_element


class HtmlFormsetFormWithPersistedDataHeading(HtmlPanelHeading):
    """
    An special panel heading with a Delete button for dynamic deletion of extra empty formset forms.
    """

    def __init__(self, delete_input_tag, **kwargs):
        super(HtmlFormsetFormWithPersistedDataHeading, self).__init__(delete_checkbox=delete_input_tag, **kwargs)

    def build(self):
        xml_element = super(HtmlFormsetFormWithPersistedDataHeading, self).build()

        delete_checkbox_wrapper = Element('div', attrib={'class': 'pull-right'})
        label = Element('label')
        label.text = '{{ %(input_tag)s }} %(label)s' % {'input_tag': self.context_arg('delete_checkbox'),
                                                        'label': _(u'Delete')}
        delete_checkbox_wrapper.append(label)

        xml_element.append(delete_checkbox_wrapper)

        return xml_element


class HtmlFormsetEmptyFormTemplate(HtmlElement):
    """
    An HtmlFormsetForm wrapped within a 'script' tag for form dynamic generation in JS.

    empty_form: value of 'empty_form' attribute of related Django Formset
    content: One or more template elements representing a form of the formset
    """

    def __init__(self, empty_form, content=None, **kwargs):
        html_formset_form = HtmlFormsetEmptyDynamicForm(empty_form=empty_form, content=content)
        super(HtmlFormsetEmptyFormTemplate, self).__init__(content=html_formset_form, **kwargs)

    def tag(self):
        return 'script'

    def attrib(self):
        return {'type': 'form-template', 'data-formset-empty-form': ''}

    def build(self):
        xml_element = self.build_xml_element()

        html_formset_form = self.content[0]
        html_formset_form.escape_script_tags()
        html_formset_form_xml_element = html_formset_form.build()
        xml_element.append(html_formset_form_xml_element)

        return xml_element


class HtmlFormsetEmptyDynamicForm(HtmlFormsetForm):
    """
    An empty formset form for forms dynamically added by JS. INTENDED TO BE USED ONLY BY HtmlFormsetEmptyFormTemplate.
    """

    def __init__(self, empty_form, **kwargs):
        self.empty_form = empty_form
        super(HtmlFormsetEmptyDynamicForm, self).__init__(**kwargs)

    def element_id(self):
        return super(HtmlFormsetEmptyDynamicForm, self).element_id() + '-__prefix__'

    def build(self):
        html_formset_form = super(HtmlFormsetEmptyDynamicForm, self).build()

        dynamic_rules_js_code = JSInline(formset_dynamic_rules(self.empty_form), escape_tag=True).render()
        delete_formset_form_js_code = JSTemplate("forms/delete_formset_form.js.html", {'element_id': self.element_id()},
                                                 escape_tag=True).render()
        html_formset_form.tail = dynamic_rules_js_code + delete_formset_form_js_code
        return html_formset_form


class SimpleHtmlFormset(HtmlFormset):
    """
    An HtmlFormset with a basic structure for simple use cases.
    """

    def __init__(self, formset):
        self._formset = formset
        content = [HtmlFormsetBody(content=self._build_html_formset_forms()),
                   HtmlFormsetEmptyFormTemplate(formset.empty_form,
                                                content=self._build_empty_group_word_form())]
        super(SimpleHtmlFormset, self).__init__(formset.prefix, formset.management_form, show_add=True, content=content)

    def html_form_content(self, form):
        """
        :param form:
        :return: Must return a list of TemplateElements with the contents of the form
        """
        raise NotImplementedError("Subclass responsibility.")

    def existing_form_label(self, form):
        raise NotImplementedError("Subclass responsibility.")

    def new_form_label(self):
        raise NotImplementedError("Subclass responsibility.")

    # private

    def _build_html_formset_forms(self):
        html_formset_forms = []
        for form in self._formset.forms:
            if form['id'].value():
                html_panel_title = TextElement(self.existing_form_label(form))
                html_panel_heading = HtmlFormsetFormWithPersistedDataHeading(
                    delete_input_tag=form['DELETE'],
                    content=html_panel_title)
                html_formset_forms.append(
                    HtmlFormsetForm(content=[html_panel_heading, HtmlPanelBody(content=self.html_form_content(form))]))
            else:
                html_formset_forms.append(HtmlFormsetEmptyForm(content=self._build_empty_group_word_form()))
        return html_formset_forms

    def _build_empty_group_word_form(self):
        return [HtmlFormsetEmptyFormHeading(content=TextElement(self.new_form_label())),
                HtmlPanelBody(content=self.html_form_content(self._formset.empty_form))]
