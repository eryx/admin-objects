from admin_objects.html_builders.base import HtmlAnchor, HtmlElement
from admin_objects.html_builders.components.list_group import HtmlListGroup


class HtmlAnchorCollapse(HtmlAnchor):
    def __init__(self, target_id, content=None, **kwargs):
        href = '#%s' % target_id
        super(HtmlAnchorCollapse, self).__init__(href=href, content=content, **kwargs)

    def attrib(self):
        attribs = super(HtmlAnchorCollapse, self).attrib()
        attribs.update({'data-toggle': 'collapse'})
        return attribs


class HtmlListGroupCollapse(HtmlListGroup):
    def __init__(self, collapsed=True, **kwargs):
        self.collapsed = collapsed
        super(HtmlListGroupCollapse, self).__init__(**kwargs)

    def css_classes(self):
        css_classes = super(HtmlListGroupCollapse, self).css_classes()
        css_classes.append('collapse')
        if not self.collapsed:
            css_classes.append('in')
        return css_classes


class HtmlPanelCollapse(HtmlElement):
    def __init__(self, collapsed=True, **kwargs):
        self.collapsed = collapsed
        super(HtmlPanelCollapse, self).__init__(**kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        css_classes = ['panel-collapse', 'collapse']
        if not self.collapsed:
            css_classes.append('in')
        return css_classes
