from xml.etree.ElementTree import Element

from admin_objects.html_builders.base import HtmlElement, HtmlListItem


class HtmlTabPanel(HtmlElement):
    def __init__(self, content):
        super(HtmlTabPanel, self).__init__(content=content)

        self.assert_only_n_contents_exist(n=2)

    def css_classes(self):
        return ['tabbable', 'tabbable-bordered']  # FIXME: Non standard Bootstrap!

    def tag(self):
        return 'div'

    def attrib(self):
        return {'role': 'tabpanel'}


class HtmlTabList(HtmlElement):
    def __init__(self, content_left, content_right=None, **kwargs):
        content_left = content_left or []
        content_right = content_right or []
        super(HtmlTabList, self).__init__(content=content_left + content_right, **kwargs)

        self.assert_content_is_not_empty()
        self.assert_contents_are_list_items()

        self.mark_first_item_as_active()
        self.set_css_classes_to_right_content(content_right)

    def assert_contents_are_list_items(self):
        for content in self.content:
            assert isinstance(content, HtmlListItem), u'Contents of HtmlTabList must be instances of HtmlListItem'

    def tag(self):
        return 'ul'

    def css_classes(self):
        return ['nav', 'nav-tabs']

    def attrib(self):
        return {'role': 'tablist'}

    def mark_first_item_as_active(self):
        for content in self.content:
            if isinstance(content, HtmlTabListItem):
                content.mark_as_active()
                return

        raise Exception(u'HtmlTabList must have at least one HtmlTabListItem')

    def set_css_classes_to_right_content(self, content_right):
        for content in content_right:
            content.add_css_classes(['pull-right'])


class HtmlTabListItem(HtmlListItem):
    def __init__(self, title, tab_pane_id, active=False, **kwargs):
        self.title = title
        self.tab_pane_id = tab_pane_id
        self.active = active

        super(HtmlTabListItem, self).__init__(title=title, **kwargs)

    def css_classes(self):
        if self.active:
            return ['active']

        return []

    def attrib(self):
        return {'role': 'presentation'}

    def build(self):
        xml_element = self.build_xml_element()

        xml_element.append(self.build_tab_anchor())

        return xml_element

    def build_tab_anchor(self):
        anchor = Element('a', attrib={'href': '#%s' % self.tab_pane_id, 'aria-controls': self.tab_pane_id,
                                      'role': 'tab', 'data-toggle': 'tab'})
        anchor.text = self.title
        return anchor

    def mark_as_active(self):
        self.active = True


class HtmlTabPanelContent(HtmlElement):
    def __init__(self, content, **kwargs):
        super(HtmlTabPanelContent, self).__init__(content=content, **kwargs)

        self.assert_content_is_not_empty()
        self.assert_contents_are_tab_panes()
        self.mark_first_pane_as_active()

    def tag(self):
        return 'div'

    def css_classes(self):
        return ['tab-content']

    def mark_first_pane_as_active(self):
        self.content[0].mark_as_active()

    def assert_contents_are_tab_panes(self):
        for content in self.content:
            assert isinstance(content, HtmlTabPane), u'Contents of HtmlTabPanelContent must be instances of HtmlTabPane'


class HtmlTabPane(HtmlElement):
    def __init__(self, active=False, **kwargs):
        self.active = active

        super(HtmlTabPane, self).__init__(**kwargs)

    def tag(self):
        return 'div'

    def css_classes(self):
        classes = ['tab-pane']

        if self.active:
            classes.append('active')

        return classes

    def attrib(self):
        return {'role': 'tabpanel'}

    def mark_as_active(self):
        self.active = True
