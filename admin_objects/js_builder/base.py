from xml.etree.ElementTree import Element, tostring

from django.conf import settings
from django.template.loader import render_to_string


class JS(object):
    """
    Abstract class representing JS scripts.

    NOTE: JS objects are rendered beforehand. This is because they can be escaped like '<script><\/script>',
          which are not valid xml Element's.
    """

    def __init__(self, escape_tag=False):
        self.script_tag_rendered = self.render_script_tag()
        self.escape_tag = escape_tag

    def render_script_tag(self):
        raise Exception("Subclass responsibility.")

    def render(self):
        script_tag_rendered = self.script_tag_rendered
        if self.escape_tag:
            script_tag_rendered = script_tag_rendered.replace('</script>', '<\/script>')
        return script_tag_rendered

    def __eq__(self, other):
        return self.script_tag_rendered == other.script_tag_rendered

    def __ne__(self, other):
        return not self.__eq__(other)


class JSFile(JS):
    def __init__(self, file_path, escape_tag=False):
        self.file_path = file_path
        super(JSFile, self).__init__(escape_tag=escape_tag)

    def render_script_tag(self):
        script_tag = Element('script', attrib={'src': settings.STATIC_URL + self.file_path})

        # Needed to force <script></script> rendering (<script /> do not work as expected in browsers apparently)
        script_tag.text = ' '

        return tostring(script_tag).decode("utf-8")


class JSInline(JS):
    """
    Inline js code for one-line code that is not worth it to put on a template
    """

    def __init__(self, js_code, escape_tag=False):
        self.js_code = js_code
        super(JSInline, self).__init__(escape_tag=escape_tag)

    def render_script_tag(self):
        script_tag = Element('script')
        script_tag.text = "$(function() { %s });" % self.js_code
        return tostring(script_tag).decode("utf-8")

    def import_once(self):
        return True


class JSTemplate(JS):
    def __init__(self, template_path, context=None, escape_tag=False):
        self.template_path = template_path
        self.context = context
        super(JSTemplate, self).__init__(escape_tag=escape_tag)

    def render_script_tag(self):
        return render_to_string(self.template_path, self.context)


class JSImporter(object):
    """
        Imports JS Files considering order
    """

    def __init__(self, js_files):
        self.js_files = js_files

    def add(self, js_importer):
        not_imported = [js_file for js_file in js_importer if
                        (js_file not in self.js_files) or isinstance(js_file, JSTemplate)]
        self.js_files = self.js_files + not_imported

    def __iter__(self):
        return self.js_files.__iter__()
