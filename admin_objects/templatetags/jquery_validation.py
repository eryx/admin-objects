# Based on https://github.com/shabble/django-jquery-form-validation

from json import JSONEncoder

from django import forms
from django import template
from django.core import validators
from django.forms.formsets import BaseFormSet
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

register = template.Library()


@register.tag()
def validation_rules(parser, token):
    """
    This will render a Django form into a jquery validate ruleset.

    Usage::

        {% validation_rules form1 form2 formset1 ... %}

    """
    bits = token.contents.split()
    args = bits[1:]

    return ValidationRulesNode(*bits[1:])


@register.tag()
def validation_messages(parser, token):
    """
    This will render the validation messages .

    Usage::

        {% validation_messages form1 form2 formset1 ... %}

    """
    bits = token.contents.split()
    args = bits[1:]

    return ValidationMessagesNode(*bits[1:])


@register.simple_tag()
def formset_dynamic_rules(formset_empty_form):
    jquery_statements = []
    for field_name, field in formset_empty_form.fields.items():
        field_id = "id_%(prefix)s-%(field_name)s" % {'prefix': formset_empty_form.prefix, 'field_name': field_name}

        rules = FieldRulesBuilder(field).build()

        if rules:
            rule_params = rules
            messages = FieldMessagesBuilder(field).build()
            rule_params['messages'] = messages

            rule_params_enc = JSONEncoder().encode(rule_params)
            jquery_statement = "$('#%(field_id)s').rules('add', %(rule_params)s);" % {'field_id': field_id,
                                                                                      'rule_params': rule_params_enc}
            jquery_statements.append(jquery_statement)

    return_jquery = ""
    for jquery_statement in jquery_statements:
        return_jquery += jquery_statement + "\n"

    return return_jquery


class MultipleArgsNode(template.Node):
    def __init__(self, *args):
        self.node_vars_list = []
        for arg in args:
            form_var = template.Variable(arg)
            self.node_vars_list.append(form_var)


class JqueryValidationBaseNode(MultipleArgsNode):
    def assert_args(self, context):
        for node_var in self.node_vars_list:
            real_arg = node_var.resolve(context)
            if not (isinstance(real_arg, forms.BaseForm) or isinstance(real_arg, BaseFormSet)):
                raise TypeError(_("Expected Django Form (or FormSet) instance"))

    def get_forms_and_formsets(self, context):
        forms_and_formsets_to_process = []

        for node_var in self.node_vars_list:
            forms_and_formsets_to_process.append(node_var.resolve(context))

        return forms_and_formsets_to_process


class ValidationRulesNode(JqueryValidationBaseNode):
    def render(self, context):
        self.assert_args(context)
        form_validation_builder = FormValidationOptionBuilder(self.get_forms_and_formsets(context))
        return form_validation_builder.build_rules()


class ValidationMessagesNode(JqueryValidationBaseNode):
    def render(self, context):
        self.assert_args(context)
        form_validation_builder = FormValidationOptionBuilder(self.get_forms_and_formsets(context))
        return form_validation_builder.build_messages()


class FormValidationOptionBuilder(object):
    def __init__(self, forms_or_formsets):
        self.forms_to_process = []
        for form_or_formset in forms_or_formsets:
            if isinstance(form_or_formset, forms.BaseForm):
                form = form_or_formset
                self.forms_to_process.append(form)
            if isinstance(form_or_formset, BaseFormSet):
                formset = form_or_formset
                for form_in_formset in formset.forms:
                    self.forms_to_process.append(form_in_formset)

    def _build(self, form, field_validation_option_builder_klass):
        components = {}
        for field_name, field in form.fields.items():
            field_component = field_validation_option_builder_klass(field).build()
            if field_component:
                if form.prefix:
                    dom_field_name = '%s-%s' % (form.prefix, field_name)
                else:
                    dom_field_name = field_name

                components[dom_field_name] = field_component

        return components

    def build_messages(self):
        messages = {}
        for form in self.forms_to_process:
            form_messages = self._build(form, FieldMessagesBuilder)
            if form_messages:
                messages.update(form_messages)
        return JSONEncoder().encode(messages)

    def build_rules(self):
        rules = {}
        for form in self.forms_to_process:
            form_rules = self._build(form, FieldRulesBuilder)
            if form_rules:
                rules.update(form_rules)
        return JSONEncoder().encode(rules)


class FieldValidationOptionBuilder(object):
    def __init__(self, field):
        self.field = field

    def build(self):
        raise Exception("Subclass responsibility.")


class FieldMessagesBuilder(FieldValidationOptionBuilder):
    def build(self):
        field_messages = {}

        # Required
        if hasattr(self.field, 'required'):
            if self.field.required:
                field_messages['required'] = self._field_error_message('required')

        if isinstance(self.field, forms.TimeField):
            field_messages['time'] = self._field_error_message('invalid')

        if isinstance(self.field, forms.DateField):
            field_messages['dateITA'] = self._field_error_message('invalid')

        if isinstance(self.field, forms.DecimalField) or isinstance(self.field, forms.IntegerField):
            field_messages['number'] = self._field_error_message('invalid')

        for validator in self.field.validators:
            # TODO
            # # Min length
            # if isinstance(validator, validators.MinLengthValidator):
            #     if self.field.min_length is not None:
            #         field_messages['minlength'] = self._field_error_message('invalid')
            # # Max length
            # if isinstance(validator, validators.MaxLengthValidator):
            #     if self.field.max_length is not None:
            #         field_messages['maxlength'] = self._field_error_message('invalid')

            # Regular expression
            if isinstance(validator, validators.RegexValidator):
                if isinstance(validator, validators.EmailValidator):
                    field_messages['email'] = self._field_error_message('invalid')
                elif isinstance(validator, validators.URLValidator):
                    field_messages['url'] = self._field_error_message('invalid')
                else:
                    field_messages['regexp'] = self._field_error_message('invalid')

        return field_messages

    def _field_error_message(self, message_name="invalid"):
        return force_text(self.field.error_messages.get(message_name))


class FieldRulesBuilder(FieldValidationOptionBuilder):
    def build(self):
        field_rules = {}

        # hidden fields shouldn't have client side rules
        if self.field.widget.is_hidden:
            return {}

        if hasattr(self.field, 'required'):
            if self.field.required:
                field_rules['required'] = True

        if isinstance(self.field, forms.TimeField):
            field_rules['time'] = True

        if isinstance(self.field, forms.DateField):
            field_rules['dateITA'] = True

        if isinstance(self.field, forms.DecimalField) or isinstance(self.field, forms.IntegerField):
            field_rules['number'] = True

        for validator in self.field.validators:
            # FIXME: Gives "Uncaught TypeError: Cannot read property 'form' of undefined"
            # # Min length
            # if isinstance(validator, validators.MinLengthValidator):
            #     if self.field.min_length is not None:
            #         field_rules['minlength'] = self.field.min_length
            #
            # # Max length
            # if isinstance(validator, validators.MaxLengthValidator):
            #     if self.field.max_length is not None:
            #         field_rules['maxlength'] = self.field.max_length

            # Regular expression

            # URL and Email fields both use a regex validator, but we
            # want to use the built-in specific validators in jquery
            # validate, not Django regexes.
            if isinstance(validator, validators.RegexValidator):
                if isinstance(validator, validators.EmailValidator):
                    field_rules['email'] = True
                elif isinstance(validator, validators.URLValidator):
                    field_rules['url'] = True
                else:
                    field_rules['regexp'] = validator.regex.pattern

        return field_rules
