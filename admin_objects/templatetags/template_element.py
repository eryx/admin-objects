from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def render_template_element(context, template_element):
    return template_element.render(request=context['request'])
