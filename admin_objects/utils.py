import calendar
import datetime
import inspect


def model_attrs_to_dict(obj, exclude=None):
    exclude = exclude or []
    return dict(
        (field.name, getattr(obj, field.name)) for field in obj.__class__._meta.fields if field.name not in exclude)


def attrs_to_dict(obj, attr_list=None):
    if not attr_list:
        result_dict = dict(
            (name, getattr(obj, name)) for name in dir(obj.__class__) if
            not inspect.ismethod(getattr(obj, name)) and not name.startswith('__'))
    else:
        result_dict = {}
        for attr in attr_list:
            if hasattr(obj, attr):
                result_dict[attr] = getattr(obj, attr)
            else:
                raise Exception("'%(attr)s' is not an attribute of %(obj)s" % {'attr': attr, 'obj': obj})
    return result_dict


def dict_to_attrs(obj, attr_dict):
    for attr, value in attr_dict.items():
        if hasattr(obj, attr):
            setattr(obj, attr, value)
        else:
            raise Exception("'%(attr)s' is not an attribute of %(obj)s" % {'attr': attr, 'obj': obj})
    return obj


def filter_and_pop_dict_attrs(dic, filter_func):
    filtered_fields = {}
    for field, value in dic.items():
        if filter_func(field):
            filtered_fields[field] = value
            dic.pop(field)

    return filtered_fields


def all_subclasses(klass):
    return klass.__subclasses__() + [g for s in klass.__subclasses__()
                                     for g in all_subclasses(s)]


def first_day_of_current_month():
    today = datetime.date.today()
    first_day_of_current_month_value = datetime.date(today.year, today.month, 1)
    return first_day_of_current_month_value


def last_day_of_current_month():
    today = datetime.date.today()
    last_day = calendar.monthrange(today.year, today.month)[1]
    last_day_of_current_month_value = datetime.date(today.year, today.month, last_day)
    return last_day_of_current_month_value
