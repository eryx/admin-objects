import json

from django.contrib import messages
from django.db import transaction
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.views.generic import View

from admin_objects.html_builders.forms_extra import HtmlFormWithValidation


class FormsView(View):
    def __init__(self, **kwargs):
        super(FormsView, self).__init__(**kwargs)
        self._forms = None
        self._forms_validator = self.__class__.validator_class()(view=self)

    def get(self, request, *args, **kwargs):
        self._forms = self.create_forms()
        return self.render_template_response(request)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self._forms = self.create_forms(data=request.POST)

        if self.has_ajax_validation(request):
            return self.ajax_form_validation()

        return self.submit_html_form(request)

    # helpers protocol

    def has_ajax_validation(self, request):
        return bool(int(request.GET.get('ajax_validation', u'0')))

    def ajax_form_validation(self):
        if self.validator().validate():
            return HttpResponse(json.dumps({}), content_type='application/json')
        else:
            response = {'errors': self.validator().all_errors(),
                        'field_errors': self.validator().field_errors()}
            return HttpResponse(json.dumps(response), content_type='application/json')

    def submit_html_form(self, request):
        if self.validator().validate():
            self.after_forms_validation_succeeded(request)
            return self.redirect_on_success(request)
        else:
            self.add_errors_as_session_messages(request, self.validator().all_errors())
            return self.render_template_response(request)

    def add_errors_as_session_messages(self, request, errors):
        for error in errors:
            messages.error(request, error)

    def render_template_response(self, request):
        context = self.template_context(request)
        context.update({'form': self.html_form()})
        return TemplateResponse(request, template=self.__class__.template(), context=context)

    def html_form(self):
        return HtmlFormWithValidation(
            content=self.html_form_content(),
            forms_validator=self.validator()
        )

    # access protocol

    def form(self, name):
        return self._forms[name]

    def forms(self):
        return self._forms

    def has_form(self, name):
        return name in self.forms()

    def validator(self):
        return self._forms_validator

    # public protocol

    @classmethod
    def validator_class(cls):
        """
        The validator class
        """
        raise NotImplementedError('Subclass responsibility.')

    @classmethod
    def template(cls):
        """
        Returns the template to use.
        """
        raise NotImplementedError('Subclass responsibility.')

    def create_forms(self, data=None):
        """
        Returns a dictonary with instanced forms.
        """
        raise NotImplementedError('Subclass responsibility.')

    def html_form_content(self):
        """
        Returns the content (children) of the HtmlForm template element (a TemplateElement list)
        """
        raise NotImplementedError('Subclass responsibility.')

    def template_context(self, request):
        """
        Additional template context passed to the view. Called before rendering the response.
        """
        return {}

    def after_forms_validation_succeeded(self, request):
        """
        Actions to be done after all forms are valid, and before redirecting.
        """
        raise NotImplementedError('Subclass responsibility.')

    def redirect_on_success(self, request):
        """
        Return a redirect response after forms validation succeeds.
        """
        raise NotImplementedError('Subclass responsibility.')
