from django.forms.forms import BaseForm, NON_FIELD_ERRORS
from django.forms.formsets import BaseFormSet
from django.utils.translation import ugettext as _


class FormsViewValidator(object):
    def __init__(self, view):
        self.view = view
        self.errors = []

    def form(self, name):
        return self.view.form(name)

    def has_form(self, name):
        return self.view.has_form(name)

    def forms(self):
        return self.view.forms()

    def forms_list(self):
        return self.forms().values()

    def add_error_message(self, message):
        self.errors.append(message)

    def add_error_messages(self, messages):
        self.errors += messages

    def add_fields_error_message(self):
        self.add_error_message(self.__class__.fields_error_message())

    def all_errors(self):
        return self.errors

    def field_errors_of_form(self, form):
        errors_per_field = {}
        for field_name, error_list in form.errors.items():
            if field_name == NON_FIELD_ERRORS:
                continue
            field_html_name = form[field_name].html_name
            errors_per_field[field_html_name] = []
            for error in error_list:
                errors_per_field[field_html_name].append(str(error))
        return errors_per_field

    def field_errors(self):
        errors_per_field = {}

        for form in self.forms_list():
            if isinstance(form, BaseFormSet):
                formset = form
                for _form in formset:
                    errors_per_field.update(self.field_errors_of_form(_form))
            elif isinstance(form, BaseForm):
                errors_per_field.update(self.field_errors_of_form(form))

        return errors_per_field

    def has_no_errors(self):
        return len(self.all_errors()) == 0

    def validate(self):
        self.validate_forms()
        if self.has_no_errors():
            self.validate_view()
        return self.has_no_errors()

    def validates_through_ajax(self):
        raise NotImplementedError('Subclass responsibility.')

    def validate_forms(self):
        raise NotImplementedError('Subclass responsibility.')

    def validate_view(self):
        raise NotImplementedError('Subclass responsibility.')

    def check_form_errors(self, form):
        if form.errors and not form.non_field_errors():
            self.add_fields_error_message()
            return True
        if form.non_field_errors():
            self.add_error_messages(form.non_field_errors())
            return True
        return False

    @classmethod
    def fields_error_message(cls):
        return _(u"Please correct errors shown in red.")


class FormsViewValidatorFull(FormsViewValidator):
    """
    Backend validator with full validation support (using AJAX for non-field errors)
    """

    def validates_through_ajax(self):
        return True

    def validate_forms(self):
        for form in self.forms_list():
            form_is_valid = form.is_valid()
            if isinstance(form, BaseForm):
                has_errors = self.check_form_errors(form)
                if has_errors:
                    return
            if isinstance(form, BaseFormSet):
                formset = form
                for _form in formset:
                    has_errors = self.check_form_errors(_form)
                    if has_errors:
                        return
                if formset.non_form_errors():
                    self.add_error_messages(formset.non_form_errors())
                    return
            if not form_is_valid:
                raise Exception("Form is invalid but no errors were catch!")

    def validate_view(self):
        pass


class FormsViewValidatorFieldsOnly(FormsViewValidator):
    """
    Backend validator for simple forms that only rely on frontend validation.
    Useful for not having to perform an addtional ajax request before save, when validation is fields-only.
    """

    def validates_through_ajax(self):
        return False

    def validate_forms(self):
        all_forms_valid = True
        for form in self.forms_list():
            all_forms_valid = all_forms_valid and form.is_valid()
            if isinstance(form, BaseForm):
                if form.errors and not form.non_field_errors():
                    self.add_fields_error_message()
                    return
            if isinstance(form, BaseFormSet):
                formset = form
                for _form in formset:
                    if _form.errors and not _form.non_field_errors():
                        self.add_fields_error_message()
                        return

        if not all_forms_valid and self.has_no_errors():
            raise Exception("You are using FormsViewValidatorFieldsOnly validator but there are non-field errors "
                            "present in at least one form. Should use standard validator instead (FormsViewValidator).")

    def validate_view(self):
        pass
