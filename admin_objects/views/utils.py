from django.db import transaction
from django.forms.formsets import DELETION_FIELD_NAME


class QuerysetTableView(object):
    def __init__(self, queryset, row_klass=None, limit=None):
        self.rows = []

        objects = queryset[:limit] if limit else queryset
        for obj in objects:
            row = row_klass.new(obj)
            self.rows.append(row)

    def __iter__(self):
        return iter(self.rows)


class ObjectRowView(object):
    @classmethod
    def new(cls, obj):
        return cls(obj)

    def __init__(self, obj):
        self.obj = obj

    def __getattr__(self, name):
        return getattr(self.obj, name)


class FormsetManager(object):
    def __init__(self, formset):
        self._formset = formset

    def new_forms(self):
        new_forms = []
        for extra_form in self._formset.extra_forms:
            if not extra_form.has_changed():
                continue

            if self._formset.can_delete and extra_form.cleaned_data.get(DELETION_FIELD_NAME, False):
                continue

            if DELETION_FIELD_NAME in extra_form.cleaned_data:
                extra_form.cleaned_data.pop(DELETION_FIELD_NAME)

            new_forms.append(extra_form)

        return new_forms

    def filled_forms(self):
        filled_forms = self._formset.initial_forms
        filled_forms += self.new_forms()
        return filled_forms

    def changed_forms(self):
        changed_forms = []
        deleted_forms = self.deleted_forms()

        for initial_form in self._formset.initial_forms:
            if initial_form.has_changed() and initial_form not in deleted_forms:
                changed_forms.append(initial_form)

        return changed_forms

    def deleted_forms(self):
        try:
            forms_to_delete = self._formset.deleted_forms
        except AttributeError:
            forms_to_delete = []

        deleted_forms = []
        for initial_form in self._formset.initial_forms:
            if initial_form in forms_to_delete:
                deleted_forms.append(initial_form)

        return deleted_forms


class InlineFormsetSaver(object):
    def __init__(self, formset, obj_klass, parent_obj, **extra_args):
        self._formset_manager = FormsetManager(formset)
        self.obj_klass = obj_klass
        self.parent_obj = parent_obj
        self.extra_args = extra_args

    def save_new_obj_form(self, new_obj_form):
        raise NotImplementedError("Subclass responsibility.")

    def save_update_obj_form(self, update_obj_form, obj_to_update):
        raise NotImplementedError("Subclass responsibility.")

    def get_parent_obj(self):
        return self.parent_obj

    @transaction.atomic
    def save_formset(self):
        for deleted_form in self._formset_manager.deleted_forms():
            obj = self.obj_klass.objects.get(pk=deleted_form.cleaned_data['id'])
            obj.delete()

        for changed_form in self._formset_manager.changed_forms():
            obj = self.obj_klass.objects.get(pk=changed_form.cleaned_data['id'])
            self.save_update_obj_form(changed_form, obj)

        for new_forms in self._formset_manager.new_forms():
            self.save_new_obj_form(new_forms)
