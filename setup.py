import version
from setuptools import setup, find_packages

setup(
    name='admin-objects',
    version=version.number,
    description='Python/Django framework that provides objects to build admin-like pages',
    packages=find_packages(),
    package_data={
        '': ['*.html']
    },
    install_requires=[
        'Django>=1.6',
        'django-widget-tweaks',
        'django-formset-js==0.4.0',
        'six',
        'future'
    ],
    include_package_data=True,
)
