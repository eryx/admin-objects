from django.contrib.messages.storage.fallback import FallbackStorage
from django.core.exceptions import ValidationError
from django.forms import Form, CharField, IntegerField, Field
from django.forms.formsets import formset_factory
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory

from admin_objects.html_builders.css.forms import HtmlInput
from admin_objects.views.base import FormsView
from admin_objects.views.forms_validator import FormsViewValidatorFull, FormsViewValidator, FormsViewValidatorFieldsOnly


class TestFormsView(TestCase):
    def setUp(self):
        self.forms_edit_view = None

    def _add_messages_framework_support(self, request):
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

    # test asserts helper protocol

    def assert_form_is_full_with_data(self, response):
        self.assertIn("Juan Perez", response.content.decode("utf-8"))
        self.assertIn("20", response.content.decode("utf-8"))

    def assert_template_render_correctly(self, response):
        self.assertEqual(response.status_code, 200)
        self.assertIn("Edit Object", response.content.decode("utf-8"))

    def assert_error_message_is_present(self, response, message):
        self.assertInHTML('<li class="error">%s</li>' % message, response.content.decode("utf-8"))

    def assert_invalid_fields_error_message_is_present(self, response):
        self.assert_error_message_is_present(response, message=FormsViewValidator.fields_error_message())

    def assert_invalid_fields_error_message_is_not_present(self, response):
        fields_error_message = '<li class="error">%s</li>' % FormsViewValidator.fields_error_message()
        self.assertNotIn(fields_error_message, response.content.decode("utf-8"))

    def assert_response_with_json_invalid_fields_error(self, response, fields_errors):
        self.assertJSONEqual(response.content.decode("utf-8"), {'errors': [FormsViewValidator.fields_error_message()],
                                                                'field_errors': fields_errors})

    def assert_response_with_json_has_error_message_with_no_field_errors(self, response, errors):
        self.assertJSONEqual(response.content.decode("utf-8"), {'errors': errors,
                                                                'field_errors': {}})

    def assert_redirect(self, response):
        self.assertTrue(isinstance(response, HttpResponseRedirect))

    # test factory helper protocol

    def create_post_request(self, data=None):
        request = RequestFactory().post('/admin2/example-forms-edit-view', data=data)
        self._add_messages_framework_support(request)
        return request

    def create_get_request(self):
        request = RequestFactory().get('/admin2/example-forms-edit-view')
        self._add_messages_framework_support(request)
        return request


class TestFormsViewFieldsOnlyValidation(TestFormsView):
    def setUp(self):
        class AForm(Form):
            name = CharField()
            age = IntegerField()

        class AFormsEditViewClass(FormsView):
            @classmethod
            def validator_class(cls):
                return FormsViewValidatorFieldsOnly

            @classmethod
            def template(cls):
                return 'example_forms_edit_view.html'

            def create_forms(self, data=None):
                return {'example_form': AForm(data=data, initial={'name': 'Juan Perez', 'age': '20'},
                                              prefix='example-form')}

            def html_form_content(self):
                example_form = self.form('example_form')
                return [HtmlInput(input_tag=example_form['name']), HtmlInput(input_tag=example_form['age'])]

            def after_forms_validation_succeeded(self, request):
                pass

            def redirect_on_success(self, request):
                return HttpResponseRedirect('http://example.com')

        self.forms_edit_view = AFormsEditViewClass.as_view()

    # tests protocol

    def test_view_is_rendered_correctly_on_get(self):
        request = self.create_get_request()
        response = self.forms_edit_view(request).render()
        self.assert_template_render_correctly(response)
        self.assert_form_is_full_with_data(response)

    def test_valid_forms_post_triggers_redirect(self):
        request = self.create_post_request(data={'example-form-name': 'Pedro Gomez',
                                                 'example-form-age': '30'})
        response = self.forms_edit_view(request)

        self.assert_redirect(response)

    def test_invalid_fields_form_post_shows_general_error_message(self):
        request = self.create_post_request(data={'example-form-name': 'Pedro Gomez',
                                                 'example-form-age': ''})
        response = self.forms_edit_view(request).render()

        self.assert_template_render_correctly(response)
        self.assert_invalid_fields_error_message_is_present(response)


class TestFormsViewWithFullValidator(TestFormsView):
    def setUp(self):

        class AFormWithCustomClean(Form):
            name = CharField()
            age = IntegerField()

            def clean(self):
                if self.cleaned_data['name'] == 'Juan Mayor' and self.cleaned_data['age'] < 18:
                    raise ValidationError("Juan Mayor cannot be underaged.")

        class AnotherFormWithCustomClean(Form):
            name = CharField()
            age = IntegerField()

            def clean(self):
                if self.cleaned_data['name'] == 'Pedro Mayor' and self.cleaned_data['age'] < 18:
                    raise ValidationError("Pedro Mayor cannot be underaged.")

        class AFormsEditViewClass(FormsView):

            @classmethod
            def validator_class(cls):
                return FormsViewValidatorFull

            @classmethod
            def template(cls):
                return 'example_forms_edit_view.html'

            def create_forms(self, data=None):
                return {'example_form': AFormWithCustomClean(data=data, initial={'name': 'Juan Perez', 'age': '20'},
                                                             prefix='example-form'),
                        'example_formset': formset_factory(AnotherFormWithCustomClean)(data=data,
                                                                                       prefix='example-formset',
                                                                                       initial=[{'name': 'Pedro Perez',
                                                                                                 'age': '20'}])}

            def html_form_content(self):
                example_form = self.form('example_form')
                return [HtmlInput(input_tag=example_form['name']), HtmlInput(input_tag=example_form['age'])]

            def after_forms_validation_succeeded(self, request):
                pass

            def redirect_on_success(self, request):
                return HttpResponseRedirect('http://example.com')

        self.forms_edit_view = AFormsEditViewClass.as_view()

    # test factory helper protocol

    def create_post_request_with_ajax_validation_enabled(self, data=None):
        request = RequestFactory().post('/admin2/example-forms-edit-view/?ajax_validation=1', data=data)
        return request

    # tests protocol

    def test_view_is_rendered_correctly_on_get(self):
        request = self.create_get_request()
        response = self.forms_edit_view(request).render()
        self.assert_template_render_correctly(response)
        self.assert_form_is_full_with_data(response)

    def test_valid_forms_post_triggers_redirect(self):
        request = self.create_post_request(data={'example-form-name': 'Juan Gomez',
                                                 'example-form-age': '30',
                                                 'example-formset-INITIAL_FORMS': '0',
                                                 'example-formset-MAX_NUM_FORMS': '1000',
                                                 'example-formset-TOTAL_FORMS': '1',
                                                 'example-formset-0-name': 'Pedro Gomez',
                                                 'example-formset-0-age': '35'})
        response = self.forms_edit_view(request)

        self.assert_redirect(response)

    def test_invalid_fields_form_shows_general_error_message(self):
        request = self.create_post_request(data={'example-form-name': 'Juan Gomez',
                                                 'example-form-age': '',
                                                 'example-formset-INITIAL_FORMS': '0',
                                                 'example-formset-MAX_NUM_FORMS': '1000',
                                                 'example-formset-TOTAL_FORMS': '1',
                                                 'example-formset-0-name': 'Pedro Gomez',
                                                 'example-formset-0-age': '35'})
        response = self.forms_edit_view(request).render()

        self.assert_template_render_correctly(response)
        self.assert_invalid_fields_error_message_is_present(response)

    def test_invalid_fields_in_formset_shows_general_error_message(self):
        request = self.create_post_request(data={'example-form-name': 'Juan Gomez',
                                                 'example-form-age': '30',
                                                 'example-formset-INITIAL_FORMS': '0',
                                                 'example-formset-MAX_NUM_FORMS': '1000',
                                                 'example-formset-TOTAL_FORMS': '1',
                                                 'example-formset-0-name': 'Pedro Gomez',
                                                 'example-formset-0-age': ''})
        response = self.forms_edit_view(request).render()

        self.assert_template_render_correctly(response)
        self.assert_invalid_fields_error_message_is_present(response)

    def test_invalid_form_with_non_field_error_shows_validation_error_message(self):
        request = self.create_post_request(data={'example-form-name': 'Juan Mayor',
                                                 'example-form-age': '15',
                                                 'example-formset-INITIAL_FORMS': '0',
                                                 'example-formset-MAX_NUM_FORMS': '1000',
                                                 'example-formset-TOTAL_FORMS': '1',
                                                 'example-formset-0-name': 'Pedro Gomez',
                                                 'example-formset-0-age': '30'})
        response = self.forms_edit_view(request).render()

        self.assert_template_render_correctly(response)
        self.assert_invalid_fields_error_message_is_not_present(response)
        self.assert_error_message_is_present(response, message=u"Juan Mayor cannot be underaged.")

    def test_invalid_formset_with_non_field_error_shows_validation_error_message(self):
        request = self.create_post_request(data={'example-form-name': 'Juan Gomez',
                                                 'example-form-age': '30',
                                                 'example-formset-INITIAL_FORMS': '0',
                                                 'example-formset-MAX_NUM_FORMS': '1000',
                                                 'example-formset-TOTAL_FORMS': '1',
                                                 'example-formset-0-name': 'Pedro Mayor',
                                                 'example-formset-0-age': '13'})
        response = self.forms_edit_view(request).render()

        self.assert_template_render_correctly(response)
        self.assert_invalid_fields_error_message_is_not_present(response)
        self.assert_error_message_is_present(response, message=u"Pedro Mayor cannot be underaged.")

    def test_invalid_form_returns_json_with_errors_when_validating_through_ajax(self):
        request = self.create_post_request_with_ajax_validation_enabled(data={'example-form-name': 'Juan Gomez',
                                                                              'example-form-age': '',
                                                                              'example-formset-INITIAL_FORMS': '0',
                                                                              'example-formset-MAX_NUM_FORMS': '1000',
                                                                              'example-formset-TOTAL_FORMS': '0'})
        response = self.forms_edit_view(request)

        fields_errors = {u'example-form-age': [Field.default_error_messages['required'][:]]}
        self.assert_response_with_json_invalid_fields_error(response, fields_errors)

    def test_invalid_form_with_non_field_error_returns_json_with_errors_when_validating_through_ajax(self):
        request = self.create_post_request_with_ajax_validation_enabled(data={'example-form-name': 'Juan Mayor',
                                                                              'example-form-age': '15',
                                                                              'example-formset-INITIAL_FORMS': '0',
                                                                              'example-formset-MAX_NUM_FORMS': '1000',
                                                                              'example-formset-TOTAL_FORMS': '1',
                                                                              'example-formset-0-name': 'Pedro Gomez',
                                                                              'example-formset-0-age': '30'})
        response = self.forms_edit_view(request)

        error_messages = [u"Juan Mayor cannot be underaged."]
        self.assert_response_with_json_has_error_message_with_no_field_errors(response, error_messages)

    def test_valid_form_returns_empty_json_response_when_validating_through_ajax(self):
        request = self.create_post_request_with_ajax_validation_enabled(data={'example-form-name': 'Juan Gomez',
                                                                              'example-form-age': '30',
                                                                              'example-formset-INITIAL_FORMS': '0',
                                                                              'example-formset-MAX_NUM_FORMS': '1000',
                                                                              'example-formset-TOTAL_FORMS': '0'})
        response = self.forms_edit_view(request)

        self.assertJSONEqual(response.content.decode("utf-8"), {})
