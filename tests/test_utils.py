import datetime
import random
import string
from functools import reduce

from django.test import TestCase
from django.utils.timezone import now
from factory.fuzzy import FuzzyDate, FuzzyDateTime

from admin_objects.utils import attrs_to_dict, dict_to_attrs


class MockObject(object):
    attr1 = 'value1'
    attr2 = 'value2'

    def __init__(self):
        self.inst_attr = 'inst_value'

    @property
    def attr3(self):
        return 'value3'

    def func(self):
        pass

    @staticmethod
    def func2(self):
        pass

    @classmethod
    def func2(cls):
        pass


class UtilsTest(TestCase):
    def test_object_and_a_valid_attribute_list_should_be_converted_to_dict_correclty(self):
        a_mock_obj = MockObject()
        res = attrs_to_dict(a_mock_obj, ['attr1', 'attr2'])
        self.assertEqual(res, {'attr1': a_mock_obj.attr1, 'attr2': a_mock_obj.attr2})

    def test_object_and_no_attribute_list_should_be_converted_to_dict_correclty(self):
        a_mock_obj = MockObject()
        res = attrs_to_dict(a_mock_obj)
        self.assertEqual(res, {'attr1': a_mock_obj.attr1, 'attr2': a_mock_obj.attr2, 'attr3': a_mock_obj.attr3})

    def test_object_and_an_attribute_list_with_an_item_not_present_should_raise_an_exception(self):
        a_mock_obj = MockObject()
        with self.assertRaises(Exception) as context:
            res = attrs_to_dict(a_mock_obj, ['non_present_attr'])
        self.assertEqual(context.exception.args[0], "'non_present_attr' is not an attribute of %s" % repr(a_mock_obj))

    def test_object_attributes_should_be_update_correctly(self):
        a_mock_obj = MockObject()
        a_mock_obj = dict_to_attrs(a_mock_obj, {'attr1': 'new_value'})
        self.assertEqual(a_mock_obj.attr1, 'new_value')
        self.assertEqual(a_mock_obj.attr2, 'value2')


def random_string(length=10, charset=None):
    charset = charset or string.ascii_letters
    return reduce(lambda x, y: x + y, [random.choice(charset) for i in range(length)], '')


def random_integer(start=1, end=10000):
    return random.randint(start, end)


def reload_model(obj):
    return obj.__class__.objects.get(pk=obj.pk)


def random_email():
    return '%s@%s.com' % (random_string(), random_string())


def random_past_date():
    return FuzzyDate(datetime.date(2008, 1, 1)).fuzz()


def random_future_date():
    return FuzzyDate(datetime.date.today(), datetime.date(datetime.date.today().year + 5, 1, 1)).fuzz()


def random_future_datetime():
    return FuzzyDateTime(now(), now() + datetime.timedelta(days=365)).fuzz()
